var acc = document.getElementsByClassName("accordion");
var i;
for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}
$(function () {
    $('.move-up').on('click', function (e) {
        var wrapper = $(this).closest('.box')
        wrapper.insertBefore(wrapper.prev())
    })
    $('.move-down').on('click', function (e) {
        var wrapper = $(this).closest('.box')
        wrapper.insertAfter(wrapper.next())
    })
})

$(".theme").click(function() {
    if (String($(this).attr("class")).includes("theme1")) {
        $("body").css("background-color", "white");
        $("accordion").css("background-color", "rgb(84, 141, 190)");
        $(".theme").removeClass("theme1");
        $(".theme").addClass("theme2");
    } else {
        $("accordion").css("background-color", "black")
        $(".theme").removeClass("theme2");
        $(".theme").addClass("theme1");
    }
})

$(".theme").click(function() {
    var container = $(".container");
    if (container.css("background-color") == "rgb(240, 240, 240)"){
        container.css("background-color","black");
    } else {
        container.css("background-color","rgb(240, 240, 240)");
    }
    var accordion = $(".accordion");
    if (accordion.css("background-color") == "rgb(84, 141, 190)"){
        accordion.css("background-color","red");
    } else {
        accordion.css("background-color","rgb(84, 141, 190)");
    }
})