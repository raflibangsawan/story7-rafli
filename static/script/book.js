function getData() {
    console.log(document.getElementById("searchBox").value);
    var request = new XMLHttpRequest();
    request.open("GET", "https://www.googleapis.com/books/v1/volumes?q=" + document.getElementById("searchBox").value, true);
    request.onload = function(){
        var booksData = JSON.parse(request.responseText);
        console.log(booksData.items[1].volumeInfo.title);
        renderHTML(booksData);
    }
    request.send();
}

function renderHTML(data) {
    document.getElementById("theTable").innerHTML = "";
    var result = "<tr><th>Sampul Buku</th><th>Judul</th><th>Penerbit</th><th>ID</th><th>Like me!</th></tr>";
    for (i=0; i < data.items.length; i++) {
        result += "<tr>";
        result += "<td><img src=" + data.items[i].volumeInfo.imageLinks.thumbnail + "></td>";
        result += "<td>" + data.items[i].volumeInfo.title + "</td>";
        result += "<td>" + data.items[i].volumeInfo.publisher + "</td>";
        result += "<td>" + data.items[i].id + "</td>";
        result += "<td><i onclick=\"like(this)\" class=\"fa fa-thumbs-up\" id=\"likeButton\"></i></td>";
        result += "</tr>";
    }
    document.getElementById("theTable").insertAdjacentHTML("beforeend", result);
}

function like(x) {
    $.ajax({
        type: "POST",
        url: "like/",
        dataType: "json",
        data: {
            'thumbnail': $(this).prev().prev().prev().prev().src,
            'title': $(this).prev().prev().prev().innerHTML,
            'publisher':$(this).prev().prev().innerHTML,
            'book_id': $(this).prev().innerHTML,
        },
    })
}

function topBooks() {
    $.ajax({
        type: "POST",
        url: "https://story7rafli.herokuapp.com/book/",
        success: function (data) {
            var mostLiked = JSON.parse(data);
            $('.modal-body').empty()
            result = '<tr><th>Sampul Buku</th><th>Judul</th><th>Penerbit</th><th>ID</th></tr>'
            for (i = 0; i < mostLiked.length; i++) {
                result += "<tr>";
                result += "<td><img src=${mostLiked[i].thumbnail}></td>";
                result += "<td>${mostLiked[i].title}</td>";
                result += "<td>${mostLiked[i].publisher}</td>";
                result += "<td>${mostLiked[i].id}</td>";
                result += "</tr>";
            }
            $('.modal-body').append(result);
        }
    });
}
