from django.shortcuts import render
from .models import Person
from .forms import Messages_Form

# Create your views here.
def forms(request):
    if request.method == 'POST':
        form = Messages_Form(request.POST)
        if form.is_valid():
            nama = request.POST['nama']
            pesan = request.POST['pesan']
            Person.objects.create(nama = nama,pesan = pesan)

    persons = Person.objects.all()
    form = Messages_Form()

    return render(request, 'forms.html', {'persons':persons, 'form':form})

def konfirmasi(request):
    if request.method == 'POST':
        form = Messages_Form(request.POST)
        return render(request, 'konfirmasi.html', {'nama':form.data['nama'], 'pesan':form.data['pesan']})

