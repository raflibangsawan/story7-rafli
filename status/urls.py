from django.urls import path
from . import views

app_name = 'status'

urlpatterns = [
    path('', views.forms, name='forms'),
    path('confirmation/', views.konfirmasi, name='confirmation'),
]