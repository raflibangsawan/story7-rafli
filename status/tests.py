from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from .models import Person
from .views import forms, konfirmasi
from .forms import Messages_Form

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.

class UrlTest(TestCase):

    def test_route_to_form_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_forms_function_used(self):
        func = resolve('/')
        self.assertEqual(func.func, forms)

    def test_form_page_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'forms.html')

class UrlConfirmationTest(TestCase):

    def test_konfirmasi_func_is_used(self):
        func = resolve('/confirmation/')
        self.assertEqual(func.func, konfirmasi)


class ModelTest(TestCase):

    def test_create_object_from_form(self):
        testPerson = Person(nama = "rafli", pesan = "test")
        testPerson.save()
        self.assertEqual(Person.objects.all().count(), 1)
    
    def test_object_name(self):
        testPerson = Person.objects.create(nama = "rafli", pesan = "test")
        self.assertEqual(testPerson.__str__(), "rafli")


class FormTest(TestCase):

    def test_form_is_valid(self):
        form_object = {'nama': 'rafli', 'pesan': 'test'}
        form = Messages_Form(data=form_object)
        self.assertTrue(form.is_valid())
    
    def test_form_is_not_valid(self):
        form = Messages_Form(data={})
        self.assertFalse(form.is_valid())

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.browser = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        time.sleep(3)

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_confirm_message(self):

        #pengisian form
        selenium = self.browser 
        selenium.get(self.live_server_url + '/')
        nama = selenium.find_element_by_name('nama')
        pesan = selenium.find_element_by_name('pesan')
        submit = selenium.find_element_by_name('submit-btn')
        nama.send_keys('rafli')
        pesan.send_keys('test')
        submit.send_keys(Keys.RETURN)
        time.sleep(3)

        #konfirmasi pesan
        self.assertIn('test', selenium.page_source)
        self.assertIn('rafli', selenium.page_source)
        time.sleep(3)

        #konfirmasi pesan
        konfirmasi = selenium.find_element_by_name('confirm')
        konfirmasi.send_keys(Keys.RETURN)
        self.assertIn('rafli', selenium.page_source)
        self.assertIn('test', selenium.page_source)
        time.sleep(3)

    def test_cancel_confirmation_message(self):
         
        #pengisian form
        selenium = self.browser 
        selenium.get(self.live_server_url + '/')
        nama = selenium.find_element_by_name('nama')
        pesan = selenium.find_element_by_name('pesan')
        submit = selenium.find_element_by_name('submit-btn')
        nama.send_keys('rafli')
        pesan.send_keys('test')
        submit.send_keys(Keys.RETURN)
        time.sleep(3)

        #konfirmasi pesan
        self.assertIn('test', selenium.page_source)
        self.assertIn('rafli', selenium.page_source)
        time.sleep(3)

        #konfirmasi pesan
        konfirmasi = selenium.find_element_by_name('cancel')
        konfirmasi.send_keys(Keys.RETURN)
        self.assertNotIn('rafli', selenium.page_source)
        self.assertNotIn('test', selenium.page_source)
        time.sleep(3)