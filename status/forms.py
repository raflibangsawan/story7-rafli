from django import forms
from .models import Person
from django.forms import ModelForm

class Messages_Form(ModelForm):
    class Meta:
        model = Person
        fields = '__all__'
