from django.db import models
from django.forms import ModelForm

# Create your models here.
class Person(models.Model):
    nama = models.CharField('Nama', max_length=30)
    pesan = models.TextField('Pesan')

    def __str__(self):
        return self.nama
