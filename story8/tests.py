from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .views import index
import time
import os

# Create your tests here.

class UrlTest(TestCase):

    def test_route_to_form_page(self):
        response = Client().get('/story8')
        self.assertEqual(response.status_code, 200)

    def test_index_function_used(self):
        func = resolve('/story8')
        self.assertEqual(func.func, index)

    def test_index_page_used(self):
        response = Client().get('/story8')
        self.assertTemplateUsed(response, 'index.html')

class AccordionTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url + '/story8')
        super(AccordionTest, self).setUp()
        
    def test_accordion_can_be_opened(self):

        self.selenium.get(self.live_server_url + '/story8')
        time.sleep(5)
        self.selenium.find_element_by_class_name('accordion').click()
        panel = self.selenium.find_element_by_class_name('panel')
        self.assertEqual(panel.value_of_css_property('display'), "block")
        self.selenium.find_element_by_class_name('accordion').click()
        self.assertEqual(panel.value_of_css_property('display'), "none")

    def test_if_accordion_can_move_down(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        selenium.find_element_by_id('down').click()

        upButtonDisplayed = selenium.find_element_by_id(
            'up').value_of_css_property('display')

        self.assertEqual(upButtonDisplayed, "block")

    def test_if_accordion_can_move_up(self):

        selenium = self.selenium

        selenium.implicitly_wait(5)

        selenium.find_element_by_id('up1').click()

        upButtonDisplayed = selenium.find_element_by_id(
            'down1').value_of_css_property('display')

        self.assertEqual(upButtonDisplayed, "block")

    def test_if_button_theme_works(self):

        selenium = self.selenium
        selenium.implicitly_wait(5)

        bgColorBefore = self.selenium.find_element_by_class_name('container').value_of_css_property('background-color')
        accordionColorBefore = self.selenium.find_element_by_class_name('accordion').value_of_css_property('background-color')
        
        selenium.find_element_by_class_name('theme').click()
        
        bgColorAfter = self.selenium.find_element_by_class_name('container').value_of_css_property('background-color')
        accordionColorAfter = self.selenium.find_element_by_class_name('accordion').value_of_css_property('background-color')
        time.sleep(3)
        self.assertNotEqual(bgColorBefore, bgColorAfter)
        time.sleep(3)
        self.assertNotEqual(accordionColorBefore, accordionColorAfter)
        
    def tearDown(self):

        self.selenium.quit()
        super(AccordionTest, self).tearDown()