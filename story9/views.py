from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import *
import json

@csrf_exempt
def index(request):
    if(request.method == "POST"):
        books = list(Book.objects.values()[:5])
        return HttpResponse(json.dumps(books))
    else:
        return render(request, 'book.html')

@csrf_exempt
def like(request):
    if (request.method == "POST"):
        book_id = request.POST['book_id']
        title = request.POST['title']
        publisher = request.POST['publisher']
        thumbnail = request.POST['thumbnail']
        book, created = Book.objects.get_or_create(book_id=book_id, title=title, publisher=publisher, thumbnail=thumbnail)
        book.likes += 1
        book.save()
        return HttpResponse(book.likes)
