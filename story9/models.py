from django.db import models

# Create your models here.
class Book(models.Model):
    book_id = models.CharField(max_length=300, primary_key=True, unique=True)
    title = models.CharField(max_length=300)
    publisher = models.CharField(max_length=300)
    thumbnail = models.CharField(max_length=300)
    likes = models.IntegerField(default=0)

    class Meta:
        ordering = ('-likes',)

    def __str__(self):
        return self.title

    objects = models.Manager()
