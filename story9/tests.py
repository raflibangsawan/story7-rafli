from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .views import *
from .models import *
import time
import os

class Story9UnitTest(TestCase):

    def test_book_page_is_exist(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_story9_app_is_using_index_func(self):
        found = resolve('/book/')
        self.assertEqual(found.func, index)

    def test_story9_app_is_using_book_html(self):
        response = Client().get('/book/')
        self.assertTemplateUsed(response, 'book.html')

    def test_like_page_url_is_exist(self):
        response = Client().post('/book/like/',
                                 {'book_id': "OyB4llvAoXQC", 
                                 'title': "Harry Potter dan pangeran berdarah-campuran", 
                                 'publisher': "J. K. Rowling", 
                                 'thumbnail': "http://books.google.com/books/content?id=OyB4llvAoXQC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"})
        self.assertEqual(response.status_code, 200)
    
    def test_like_func_is_used(self):
        found = resolve('/book/like/')
        self.assertEqual(found.func, like)

    def test_create_book(self):
        Book.objects.create(
            book_id= "OyB4llvAoXQC", 
            title= "Harry Potter dan pangeran berdarah-campuran", 
            publisher= "J. K. Rowling", 
            thumbnail= "http://books.google.com/books/content?id=OyB4llvAoXQC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
            )
        count = Book.objects.all().count()
        self.assertEqual(count, 1)

class Story9FuncTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url + '/book')
        super(Story9FuncTest, self).setUp()

    def test_search_book_works(self):
        selenium = self.selenium
        time.sleep(3)
        search = selenium.find_element_by_id("searchBox")
        button = selenium.find_element_by_id("sendButton")
        search.send_keys("Harry")
        button.click()

        time.sleep(5)

        self.assertIn('Harry', selenium.page_source)


    def tearDown(self):

        self.selenium.quit()
        super(Story9FuncTest, self).tearDown()