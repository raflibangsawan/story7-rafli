from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from .forms import *

def logIn(request):
    form = Login_Form()
    if(request.method == "POST"):
        form = Login_Form(request.POST)
        if(form.is_valid()):
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if(user is not None):
                login(request, user)
                return redirect('/login/')
            else:
                messages = ['Incorrect username or password!']
                response = {'form': form, 'messages': messages}
                return render(request, 'login.html', response)
    else:
        response = {'form': form}
        return render(request, 'login.html', response)


def logOut(request):
    logout(request)
    return redirect('/login/')


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/login/')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})
