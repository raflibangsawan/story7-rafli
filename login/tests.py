from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

from .models import *
from .views import *

import time
import os

class LoginUnitTest(TestCase):

    def test_app_routing_success(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_function_is_used(self):
        found = resolve('/login/')
        self.assertEqual(found.func, logIn)

    def test_app_is_using_correct_login_html(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_signup_function_is_used(self):
        found = resolve('/login/signup/')
        self.assertEqual(found.func, signup)

    def test_app_is_using_correct_signup_html(self):
        response = Client().get('/login/signup/')
        self.assertTemplateUsed(response, 'signup.html')

class LoginFunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url + '/login/')
        super(LoginFunctionalTest, self).setUp()

    def test_login_logout_signup(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        selenium.find_element_by_id('sign-up').click()

        selenium.implicitly_wait(5)

        selenium.find_element_by_id('id_username').send_keys("usernametest")
        selenium.find_element_by_id('id_password1').send_keys("passwordtest")
        selenium.find_element_by_id('id_password2').send_keys("passwordtest")
        selenium.find_element_by_id('submit-button').click()

        selenium.implicitly_wait(10)

        selenium.find_element_by_id('logout').click()

        selenium.implicitly_wait(5)

        selenium.find_element_by_id('id_username').send_keys("usernametest")
        selenium.find_element_by_id('id_password').send_keys("wrongpass")

        selenium.implicitly_wait(5)

        selenium.find_element_by_id('id_username').clear()
        selenium.find_element_by_id('id_password').clear()

        selenium.implicitly_wait(5)

        selenium.find_element_by_id('id_username').send_keys("usernametest")
        selenium.find_element_by_id('id_password').send_keys("passwordtest")

        selenium.implicitly_wait(5)

        selenium.find_element_by_id('submit').click()

        selenium.implicitly_wait(5)

        self.assertEqual(selenium.find_element_by_id('username-display').text, "Welcome, usernametest")

    def tearDown(self):
        self.selenium.quit()
        super(LoginFunctionalTest, self).tearDown()

